#!/bin/sh

# Takes arguments of "cloned directory" "git url" "thing to check out (eg. a tag)"
set -e

URL=$2
NAME=$1
REF=$3

cd /quicklisp/local-projects

git clone $URL

cd $NAME

git checkout $REF
