(defpackage :openapi.http-conditions
  (:use :cl)
  (:export
   #:message-of
   #:code-of
   #:name-of
   #:not-found
   #:forbidden
   #:unauthorized
   #:bad-request
   #:method-not-allowed
   #:proxy-authentication-required
   #:request-timeout
   #:conflict
   #:gone
   #:length-required
   #:precondition-failed
   #:payload-too-large
   #:uri-too-long
   #:unsupported-media-type
   #:expectation-failed
   #:im-a-teapot
   #:misdirected-request
   #:unprocessable-entity
   #:unavailable-for-legal-reasons
   #:request-header-fields-too-large
   #:too-many-requests
   #:precondition-required
   #:upgrade-required
   #:too-early
   #:failed-dependency
   #:locked
   #:http-condition
   #:4xx-condition))

(in-package :openapi.http-conditions)

(define-condition http-condition ()
  ((code :allocation :class
         :type (integer 300 599)
         :reader code-of)
   (name :allocation :class
         :type string
         :reader name-of)
   (message :initarg :message
            :type string
            :reader message-of)))


(defmethod jojo:%to-json ((condition http-condition))
  (jojo:with-object
    (jojo:write-key-value "error" (name-of condition))
    (jojo:write-key-value "code" (code-of condition))
    (when (slot-boundp condition 'message)
      (jojo:write-key-value "message" (message-of condition)))))


(define-condition 4xx-condition (http-condition) ())

(macrolet ((def-4xx (name code)
             `(define-condition ,name (4xx-condition)
                ((code :initform ,code)
                 (name :initform ,(format nil "~{~@(~a~)~^ ~}" (str:split #\- (string name))))))))

  (def-4xx bad-request 400)
  (def-4xx unauthorized 401)
  (def-4xx payment-required 402)
  (def-4xx forbidden 403)
  (def-4xx not-found 404)
  (def-4xx method-not-allowed 405)
  (def-4xx not-acceptable 406)
  (def-4xx proxy-authentication-required 407)
  (def-4xx request-timeout 408)
  (def-4xx conflict 409)
  (def-4xx gone 410)
  (def-4xx length-required 411)
  (def-4xx precondition-failed 412)
  (def-4xx payload-too-large 413)
  (def-4xx uri-too-long 414)
  (def-4xx unsupported-media-type 415)
  (def-4xx range-not-satisfiable 416)
  (def-4xx expectation-failed 417)
  (def-4xx im-a-teapot 418)
  (def-4xx misdirected-request 419)
  (def-4xx unprocessable-entity 422)
  (def-4xx locked 423)
  (def-4xx failed-dependency 424)
  (def-4xx too-early 425)
  (def-4xx upgrade-required 426)
  (def-4xx precondition-required 428)
  (def-4xx too-many-requests 429)
  (def-4xx request-header-fields-too-large 431)
  (def-4xx unavailable-for-legal-reasons 451))
