(defpackage :openapi/client
  (:use :cl :alexandria)
  (:local-nicknames (:http-conditions :openapi.http-conditions))
  (:export #:api-request
           #:api-request-url
           #:api-request-method
           #:api-request-body
           #:api-request-query-params
           #:api-request-content-type
           #:api-request-headers

           #:api-response
           #:make-api-response

           #:build-client
           #:make-api-call))

(in-package :openapi/client)


(defclass openapi/client (asdf:file-component)
  ((package :initarg :package
            :reader package-of)))


;;; Note: The format string seems to really mess with emacs/sly.  I have to select this whole method and C-c C-C to compile it without weird errors or the next method getting compiled instead.
(defmethod asdf:perform ((op asdf:compile-op) (c openapi/client))
  (uiop:with-temporary-file (:stream stream
                             :pathname pathname
                             :prefix (asdf:component-name c)
                             :type "lisp"
                             :direction :output)
    (format stream "
(defpackage :~(~a~)
  (:use :cl :openapi/client))

~Cin-package :~(~a~))

(build-client ~S)
"

            (package-of c)
            #\(;; stupid hack to stop sly from thinking I'm in the :~(~a package...
            (package-of c)
            (asdf:component-pathname c))
    :close-stream
    (load pathname)))

(defmethod asdf:perform ((op asdf:load-op) (c openapi/client)))


(defstruct api-request
  (method :get :type (member :get :post :put :patch :delete))
  (url nil :type string)
  (body nil :type (or null string))
  (query-params nil :type (trivial-types:association-list (or string number) (or string numbre)))
  (content-type "application/json; charset=utf-8")
  (headers nil :type (trivial-types:association-list string string)))


(defstruct api-response
  status
  content-type
  body
  headers)


(defmacro build-client (pathname)
  (multiple-value-bind (paths security-schemes base-url)
      (openapi.groveller::grovell-file pathname)
    (declare (ignore security-schemes))

    `(progn
       (export (defvar ,(intern "*API-KEY*") nil))
       (export (defvar ,(intern "*BASE-URL*") ,base-url))


       ,@(mapcar #'make-api-call
                 (hash-table-values paths)))))


(defun make-lambda-list (rpc-spec)
  `,(mapcar (lambda (param)
              (intern (openapi.groveller:rpc-parameter-spec-name param) *package*))
            (openapi.groveller:rpc-spec-parameters rpc-spec)))


(defun make-rpc-call-url (rpc-spec)
  (if (null (openapi.groveller:rpc-spec-path-variables rpc-spec))
      (first (openapi.groveller:rpc-spec-path-segments rpc-spec))
      `(format nil ,(str:join "~a" (openapi.groveller:rpc-spec-path-segments rpc-spec))
               ,@(mapcar (lambda (string) (intern string *package*))
                         (openapi.groveller:rpc-spec-path-variables rpc-spec)))))


(defun make-parameter-alist-for (in rpc-spec)
  (check-type in (member :body :header :query :path))

  `(list ,@(mapcar (lambda (param)
                   `(cons ,(openapi.groveller:rpc-parameter-spec-data-name param)
                          ,(intern (openapi.groveller:rpc-parameter-spec-name param) *package*)))
                 (remove-if-not (lambda (param-in) (eq in param-in))
                                (openapi.groveller:rpc-spec-parameters rpc-spec)
                                :key #'openapi.groveller:rpc-parameter-spec-in))))


(defun make-headers (rpc-spec)
  ''((:content-type . "application/json; charset=utf-8")))


(defun make-backend-call (rpc-spec)
  (declare (ignore rpc-spec))
  (find-symbol "REQUEST" (find-package :openapi/client/dex)))


(defun make-body-generator (rpc-spec)
  (let ((params-alist (make-parameter-alist-for :body rpc-spec)))
    (unless (equal params-alist '(list))
      `(jojo:with-output-to-string*
         (jojo:to-json
          ,params-alist)))))


(defun make-query-params (rpc-spec)
  (if-let ((security (openapi.groveller:rpc-spec-security rpc-spec)))
    (cond
      ((and (eq (openapi.groveller:security-spec-type security) :api-key)
            (eq (openapi.groveller:security-spec-in security) :query))
       (list* 'list (list 'cons (openapi.groveller:security-spec-name security)
                          (intern "*API-KEY*"))
              (cdr (make-parameter-alist-for :query rpc-spec))))
      (t
       (error "Security type ~a unimeplemented."
              (openapi.groveller:security-spec-type security))))
    (make-parameter-alist-for :query rpc-spec)))


(defun make-content-type (rpc-spec)
  "application/json; charset=utf-8")


(defun make-api-call (rpc-spec)
  (with-gensyms (response api-request)
    `(export
      (defun ,(intern (openapi.groveller:rpc-spec-identifier rpc-spec) *package*)
          ,(make-lambda-list rpc-spec)
        ;; docstring
        ,(when (openapi.groveller:rpc-spec-description rpc-spec)
           (openapi.groveller:rpc-spec-description rpc-spec))

        ;; ,(when (has-security-p spec)
        ;;    (make-security-assert spec))

        (let* ((,api-request (make-api-request
                              :url (format nil "~a~a"
                                           ,(intern "*BASE-URL*" *package*)
                                           ,(make-rpc-call-url rpc-spec))
                              :body ,(make-body-generator rpc-spec)
                              :query-params ,(make-query-params rpc-spec)
                              :content-type ,(make-content-type rpc-spec)
                              :headers ,(make-headers rpc-spec))))

          ;; (,(make-response-checker rpc-spec) ,response)

          (,(make-backend-call rpc-spec) ,api-request))))))

(import 'openapi/client (find-package :asdf))
