(defpackage :openapi/client/dex
  (:local-nicknames (:client :openapi/client))
  (:use :cl)
  (:export #:request))

(in-package :openapi/client/dex)


(defun request (api-request)
  ;; FIXME: This should never raise an error for an http code, they should just be passed out through the api-response object.
  (multiple-value-bind (body status headers)
      (dex:request (quri:render-uri
                    (quri:copy-uri
                     (quri:uri (client:api-request-url api-request))
                     :query (quri:url-encode-params
                             (client:api-request-query-params api-request))))
                   :method (client:api-request-method api-request)
                   :content (client:api-request-body api-request)
                   :headers (client:api-request-headers api-request))
    (openapi/client:make-api-response :status status
                                      :body body
                                      :headers headers)))
