(defpackage openapi.groveller
  (:use :cl :alexandria)
  (:import-from :openapi.utils
                :aget)
  (:export #:rpc-spec
           #:rpc-spec-identifier
           #:rpc-spec-method
           #:rpc-spec-path-segments
           #:rpc-spec-path-variables
           #:rpc-spec-parameters
           #:rpc-spec-produces
           #:rpc-spec-consumes
           #:rpc-spec-description
           #:rpc-spec-security

           #:rpc-parameter-spec
           #:rpc-parameter-spec-schema
           #:rpc-parameter-spec-name
           #:rpc-parameter-spec-data-name
           #:rpc-parameter-spec-in
           #:rpc-parameter-spec-description

           #:security-spec
           #:security-spec-in
           #:security-spec-name
           #:security-spec-type))

(in-package :openapi.groveller)


(defparameter +schemes-field+ (make-instance 'sanity-clause.field:member-field :members '(:http :https :ws :wss) :required t))

;; (defparameter +hash-string-field+ (sanity-clause:make-field :hash :element-type :string))

;; (defclass swagger-api-v2 ()
;;   ((swagger :data-key "swagger"
;;             :field-type :constant
;;             :constant 2.0
;;             :test #'=)
;;    (consumes :data-key "consumes"
;;              :field-type :list
;;              :element-type :string)
;;    (definitions :data-key "definitions"
;;                 :field-type :nested
;;                 :element-type (:hash :element-type :string))
;;    (host :data-key "host"
;;          :type string)
;;    (info :data-key "info"
;;          :field-type (:hash :element-type :string))
;;    (paths :data-key "paths"
;;           :field-type (:hash :element-type :string))
;;    (produces :data-key "produces"
;;              :field-type string)
;;    (schemes :data-key "schemes"
;;             :field-type :list
;;             :element-type +schemes-field+))
;;   (:metaclass sanity-clause.metaclass:validated-metaclass)
;;   (:flow :load)
;;   (:required t))


;; (defclass swagger-info-v2 ()
;;   ((contact))
;;   (:metaclass sanity-clause.metaclass:validated-metaclass))

(defstruct rpc-spec
  identifier
  (method :get :type (member :get :post :put :patch :delete))
  path-segments
  path-variables
  parameters
  produces
  consumes
  description
  security)


(defstruct security-spec
  (type :api-key :type (member :api-key :http :oauth2 :open-id-connect))
  name
  in)

(defun grovell-security-schemes (document)
  (let ((accumulator (make-hash-table :test #'equal)))
    (when-let ((components (gethash "components" document)))
      (when-let ((security-schemes (gethash "securitySchemes" components)))
        (maphash (lambda (scheme spec)
                   (setf (gethash scheme accumulator)
                         (make-security-spec :type (eswitch ((gethash "type" spec)
                                                             :test #'string=)
                                                     ("apiKey" :api-key)
                                                     ("http" :http)
                                                     ("oauth2" :oauth2)
                                                     ("openIdConnect" :open-id-connect))
                                             :name (gethash "name" spec)
                                             :in (find-location (gethash "in" spec)))))
                 security-schemes)

        (when-let ((security (gethash "security" document)))
          ;; set a default security scheme
          (setf (gethash t accumulator)
                (gethash (first (hash-table-keys (first security))) accumulator)))))
    accumulator))


;; FIXME: replace this hacky access stuff with sanity-clause
(defun grovell-file (pathname)
  ;; (let ((file-type (pathname-type pathname)))
  ;;   (cond
  ;;     ((member file-type '("yaml" "yml") :test #'string-equal)
  ;;      (yaml:parse pathname))
  ;;     ((string-equal file-type "json")
  ;;      (jonathan:parse (alexandria:read-file-into-string pathname)))))

  (let* ((accumulator (make-hash-table :test #'equal))
         (document (yaml:parse pathname))
         (security-schemes (grovell-security-schemes document))
         (base-url (when-let ((first-server (first (gethash "servers" document))))
                     (gethash "url" first-server))))

    (maphash (lambda (k v)
               (dolist (operation (grovell-paths k (hash-table-alist v) security-schemes))
                 (setf (gethash (rpc-spec-identifier operation) accumulator)
                       operation)))
             (gethash "paths" document))

    (values accumulator security-schemes base-url document)))


(defun find-verb (verb-string)
  (find verb-string '(:get :post :put :patch :delete) :test #'string-equal))


(defun grovell-paths (path paths-alist security-schemes)
  (multiple-value-bind (path-segments path-variables) (openapi.requests:interpolated-path path)
    (labels ((make-spec (verb spec-alist)
               (make-rpc-spec
                :identifier (openapi.utils:kebab-identifier (aget "operationId" spec-alist))
                :path-segments path-segments
                :path-variables path-variables
                :parameters (grovell-parameters (aget "parameters" spec-alist))
                :consumes (aget "consumes" spec-alist)
                :produces (aget "produces" spec-alist)
                :method (find-verb verb)
                :description (aget "description" spec-alist)
                :security (get-security spec-alist)))
             (get-security (spec-alist)
               (if-let ((security (aget "security" spec-alist)))
                 (gethash (first (hash-table-keys (first security))) security-schemes)
                 (gethash t security-schemes))))

      (loop for (verb . spec) in paths-alist
            collecting (make-spec verb (hash-table-alist spec))))))


(defun find-location (in-string)
  (find in-string '(:path :query :form-data :body :route) :test #'string-equal))


(defstruct rpc-parameter-spec
  schema
  name
  data-name
  (in :query :type (member :path :query :form-data :body :route))
  description)


(defun grovell-parameters (params)
  (flet ((make-param (spec)
           (make-rpc-parameter-spec
            :name (openapi.utils:kebab-identifier (aget "name" spec))
            :data-name (aget "name" spec)
            :schema (aget "schema" spec)
            :in (find-location (aget "in" spec))
            :description (aget "description" spec))))
    (mapcar (compose #'make-param #'hash-table-alist) params)))


;; (defun make-rpc-call-url (rpc-spec)
;;   (flet ((rpc-call-path-format-string ()
;;            (format nil "~A~A"))))
;;   (if-let ((query-params (remove-if-not (lambda (in) (string= "query" in)) (rpc-spec-parameters rpc-spec) :key rpc-parameter-spec-in)))
;;     (lambda (&key)
;;       (str:concat *base-path* (rpc-spec-path-segments)) )))


;; (defun make-rpc-call-lambda (rpc-spec)
;;   `(lambda (&key ,@(mapcar (compose 'openapi.utils:kebab-identifier 'rpc-parameter-spec-name) (rpc-spec-parameters rpc-spec)))

;;      (,(find-symbol (rpc-spec-method rpc-spec) (find-package :dexador))
;;       ,(make-))))
