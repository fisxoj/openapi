(defpackage openapi.requests
  (:use :cl :alexandria)
  (:export #:interpolated-path))

(in-package :openapi.requests)

(deftype parameter-location ()
  '(member :query :path :form-data :body :header))

(defun interpolated-path (path)
  "Pull interpolation values out of a path string so they can be put back together, later."

  (loop
    for search-start = 0 then end-brace
    for start-brace = (position #\{ path :test #'char= :start search-start)
    for end-brace = (when start-brace
                      (position #\} path :test #'char= :start start-brace))

    while start-brace

    when (and start-brace end-brace)
      ;; collect variable name
      collect (string-upcase
               (openapi.utils:kebab-identifier
                (subseq path (1+ start-brace) end-brace))) into variables
      ;; and path string up to this point
      and collect (subseq path (if (zerop search-start)
                                   search-start
                                   (1+ search-start))
                          start-brace)
            into path-segments

    ;; Collect any remaining part of the string as a final path segment
    finally (return (values (append path-segments (list (subseq path (if (zerop search-start)
                                                                         search-start
                                                                         (1+ search-start)))))
                            variables))))


(defmacro make-path-interpolation-function (path-string)
  (multiple-value-bind (path-segments variable-strings)
      (interpolated-path path-string)

    (let ((vars (mapcar (lambda (v) (intern (string-upcase v) *package*)) variable-strings)))
      `(lambda ,vars ,(if vars
                          `(format nil ,(str:join "~a" path-segments) ,@vars)
                          (car variable-strings))))))
