(defpackage :openapi/server
  (:use :cl :alexandria)
  (:local-nicknames (:http-conditions :openapi.http-conditions))
  (:export #:register-backend
           #:define-handler
           #:*component*
           #:package-of
           #:create-package-p-of
           #:find-component-for-package))

(in-package :openapi/server)

(defvar *component* nil
  "The asdf component being processed.  Provides some useful context like the package name.")

(defvar *package-name-to-component* (make-hash-table :test 'equal)
  "Hash table mapping package name to asdf component for that package.")

(defvar *package-name-to-operations* (make-hash-table :test 'equal)
  "Hash table mapping package name to a hash table of operations for that package.")

(defvar *known-operations* nil
  "A hash table of operations stored by id.  These can be paired with definitions to create functions endpoints that are callable by the server.")

(defvar *preamble-function* nil)

(defvar *postscript-function* nil)

(defvar *add-route-function* nil)

(defvar *callable-transform-function* nil)

(defvar *param-getter* nil)

(defclass app (asdf:file-component)
  ((type :initform "yaml")
   (package :initarg :package
            :reader package-of)
   (create-package-p :initarg :create-package
                     :reader create-package-p-of)
   (%spec-document :accessor %spec-document-of
                   :documentation "The entire parsed spec to serve as reference for schema definitions/json pointer references.")))


(define-condition coercion-failure ()
  ((parameter-name :initarg :name)
   (desired-type :initarg :type))

  (:report (lambda (c stream)
             (format stream "Failed to convert parameter ~S to type ~S."
                     (slot-value c 'parameter-name)
                     (slot-value c 'desired-type)))))

;; FIXME: stolen from json-schema... wish hash tables were print/readable...
(defun unhash (data)
  (typecase data
    (hash-table
     `(json-schema.parse:parse ,(st-json:write-json-to-string data)))

    (list
     `(list ,@(mapcar #'unhash data)))

    (t data)))


(defun call-hook (hook-var &rest args)
  (when hook-var
    (apply hook-var args)))


(defun find-operation-for-package (operation-id)
  (when-let ((operation-hash (gethash (package-name *package*) *package-name-to-operations*)))
    (gethash operation-id operation-hash)))


(defun find-component-for-package (package-name)
  (gethash (etypecase package-name
             (package (package-name package-name))
             ((or string symbol) package-name))
           *package-name-to-component*))


(defmethod asdf:perform ((o asdf:load-op) (c app))
  (let ((*component* c))
    (multiple-value-bind (paths security-schemes base-url document)
        (openapi.groveller::grovell-file (asdf:component-pathname c))
      (declare (ignore security-schemes base-url))

      (setf (gethash (string-upcase (package-of c)) *package-name-to-operations*)
            paths
            (gethash (string-upcase (package-of c)) *package-name-to-component*)
            c
            (%spec-document-of c)
            document))

    (when (create-package-p-of c)
      (uiop:ensure-package (string-upcase (package-of *component*))
                           :use '(:cl)
                           :import-from '((:openapi/server :define-handler))))

    (let ((*package* (find-package (string-upcase (package-of *component*)))))
      (call-hook *preamble-function*)
      (call-hook *postscript-function*))))


(defmethod asdf:perform ((o asdf:compile-op) (c app)))


(defun make-variable-getter (rpc-parameter-spec)
  `(,*param-getter*
    ,(openapi.groveller:rpc-parameter-spec-in rpc-parameter-spec)
    ,(openapi.groveller:rpc-parameter-spec-name rpc-parameter-spec)
    ,(gethash "default" (openapi.groveller:rpc-parameter-spec-schema rpc-parameter-spec))))


(defun angry-parser (value)
  (handler-case (parse-integer value)
    (parse-error (e)
      (declare (ignore e))
      (error 'http-conditions:bad-request
             :message (format nil "unable to parse value ~S as an integer."
                              value)))))


(defun make-coercer-for-spec (rpc-parameter-spec)
  ;; We only need to coerce parameters that come in stringified forms, ie. path or query
  (if (member (openapi.groveller:rpc-parameter-spec-in rpc-parameter-spec)
              '(:query :path))

      (if-let ((type-string (gethash "type" (openapi.groveller:rpc-parameter-spec-schema rpc-parameter-spec))))

        (eswitch (type-string :test #'string=)
          ("integer" 'angry-parser)
          ("string" 'identity))
        'identity)
      'identity))


(defun make-validator-for-spec (rpc-parameter-spec)
  (with-gensyms (value)
    `(let ((,value (,(make-coercer-for-spec rpc-parameter-spec) ,(make-variable-getter rpc-parameter-spec))))
       (multiple-value-bind (valid-p errors)
           (json-schema::validate
            ,(unhash (openapi.groveller:rpc-parameter-spec-schema rpc-parameter-spec))
            ,value
            :schema-version :draft4
            (%spec-document-of (find-component-for-package ,(package-name *package*))))
         (unless valid-p
           (error 'http-conditions:bad-request
                  :message errors)))
       ;; Finally, return the value
       ,value)))


(defun make-parameter-binding (rpc-parameter-spec)
  `(,(intern (string-upcase
              (openapi.groveller:rpc-parameter-spec-name rpc-parameter-spec)))
    ,(make-validator-for-spec rpc-parameter-spec)))


(defmacro define-handler (operation-id lambda-list &body body)
  (multiple-value-bind (body decls docs) (alexandria:parse-body body)
    (let ((rpc-spec (find-operation-for-package (string-upcase operation-id))))
      (assert (set-equal lambda-list (mapcar
                                      #'openapi.groveller:rpc-parameter-spec-name
                                      (openapi.groveller:rpc-spec-parameters rpc-spec))
                         :test #'string-equal)
              nil
              "Function arguments aren't the same as the arguments defined for the operation.")

      `(progn
         ,(call-hook
           *callable-transform-function*
           `(defun ,operation-id ()
              ,docs
              (handler-case
                  (let ,(mapcar #'make-parameter-binding
                                (openapi.groveller:rpc-spec-parameters rpc-spec))
                    ,@decls
                    ,@body)
                (http-conditions:http-condition (condition)
                  (list (http-conditions:code-of condition)
                        '(:content-type "application/json; charset=utf8")
                        (list (jojo:to-json condition)))))))

         (,*add-route-function* ,rpc-spec ,operation-id)))))


(defun register-backend (&key preamble postscript param-getter add-route callable-transform)
  (setf *preamble-function* preamble
        *postscript-function* postscript
        *param-getter* param-getter
        *add-route-function* add-route
        *callable-transform-function* callable-transform))
