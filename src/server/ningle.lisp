(defpackage openapi/server/ningle
  (:use :cl :alexandria)
  (:import-from :openapi/server
                #:*component*)
  (:import-from :assoc-utils
                #:aget))

(in-package :openapi/server/ningle)

(defvar *captured-params* nil
  "The params var that is passed to the function dispatched to by ningle.")


(defun preamble ()
  (when (openapi/server:create-package-p-of *component*)
    (setf (symbol-value (intern (string-upcase (app-symbol-of *component*)) *package*))
       (make-instance 'ningle:app))))


(defun rewrite-route-for-ningle (rpc-spec)
  (declare (type openapi.groveller:rpc-spec rpc-spec))

  (str:join
   ""
   (loop for path-segment in (openapi.groveller:rpc-spec-path-segments rpc-spec)
         for index from 0 by 1
         for variable = (nth index (openapi.groveller:rpc-spec-path-variables rpc-spec))

         collect path-segment
         when variable
           collect (str:concat ":" variable))))


(defun transform-callable (whole)
  (assert (null (third whole)) nil "The lambda list should be null.")
  (multiple-value-bind (body decls doc) (parse-body (nthcdr 3 whole) :documentation t)
    (let ((params-sym (gensym "PARAMS")))
      `(,(first whole) ,(second whole) (,params-sym)
        ,doc
        (let ((*captured-params* ,params-sym))
          ,@decls
          ,@body
          )))))


(defun find-app-symbol (component)
  (find-symbol (string-upcase (app-symbol-of component))
               (find-package (string-upcase (openapi/server:package-of component)))))


(defmacro add-route (rpc-spec function-designator)
  `(setf (ningle:route ,(find-app-symbol (openapi/server:find-component-for-package *package*))
                       ,(rewrite-route-for-ningle rpc-spec)
                       :method ,(openapi.groveller:rpc-spec-method rpc-spec))
         (function ,function-designator)))


(defun get-variable (place name &optional default)
  (ecase place
    (:query
     (let ((assoc-utils:*assoc-test* #'string-equal))
       (aget (lack.request:request-query-parameters ningle:*request*) name default)))
    (:path
     (aget *captured-params* (make-keyword name) default))
    (:body
     (aget (lack.request:request-body-parameters ningle:*request*) name default))
    (:form-data
     (aget (lack.request:request-body-parameters ningle:*request*) name default))))


(openapi/server:register-backend
 :callable-transform #'transform-callable
 :param-getter 'get-variable
 :preamble #'preamble
 :add-route 'add-route)


(defclass openapi/server/ningle (openapi/server::app)
  ((app-symbol :initarg :app-symbol
               :initform (symbol-name (gensym "APP"))
               :reader app-symbol-of)))

(import 'openapi/server/ningle 'asdf)
