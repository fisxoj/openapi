(defpackage openapi.utils
  (:use :cl :alexandria)
  (:export #:kebab-identifier
           #:aget))

(in-package :openapi.utils)


(defun aget (item alist &optional default)
  (or (cdr (assoc item alist :test #'equal))
      default))


(defun kebab-identifier (identifier)
  (with-output-to-string (s)
    (loop for char across identifier
          for first-char-p = t then nil
          do (cond
               ((upper-case-p char) (format s "~@[-~*~]~@(~A~)" (not first-char-p) char))
               ((char= #\_ char) (princ #\- s))
               (t (princ (char-upcase char) s))))))
