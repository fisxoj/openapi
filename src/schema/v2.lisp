(defpackage openapi/schema/v2
  (:use :cl :alexandria)
  (:export #:info-object)
  (:documentation "OpenAPI v2 spec classes generated from `<https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md>_`."))

(in-package :openapi/schema/v2)

(defclass contact-object ()
  ((name :type string
     :initarg :name
         :documentation "The identifying name of the contact person/organization.")
   (url :type string
        :field-type :uri
        :initarg :url
        :documentation "The URL pointing to the contact information. MUST be in the format of a URL.")
   (email :type string
          :field-type :email
          :initarg :email
          :documentation "The email address of the contact person/organization. MUST be in the format of an email address."))
  (:metaclass sanity-clause:validated-metaclass))


(defclass license-object ()
  ((name :type string
         :initarg :name
         :documentation "The license name used for the API.")
   (url :type string
        :field-type :uri
        :initarg :url
        :documentation "A URL to the license used for the API. MUST be in the format of a URL."))
  (:metaclass sanity-clause:validated-metaclass))


(defclass info-object ()
  ((title :type string
          :data-key "title"
          :initarg :title
          :required t
          :documentation "The title of the application.")
   (description :type string
                :initarg :description
                :documentation "A short description of the application. GFM syntax can be used for rich text representation.")
   (terms-of-service :type string
                     :data-key "termsOfService"
                     :initarg :terms-of-service
                     :documentation "The Terms of Service for the API.")
   (contact :type contact-object
            :field-type :nested
            :element-type contact-object
            :initarg :contact
            :documentation "The contact information for the exposed API.")
   (license :type license-object
            :field-type :nested
            :element-type license-object
            :initarg :license
            :documentation "The license information for the exposed API.")
   (version :type string
            :initarg :version
            :documentation "Provides the version of the application API (not to be confused with the specification version)."
            :required t))
  (:metaclass sanity-clause:validated-metaclass))


(defclass path-item-object (map-field)
  ()
  (:metaclass sanity-clause:validated-metaclass))


(defclass scheme-field (sanity-clause.field:member-field)
  ()
  (:default-initargs :members '("ws" "wss" "http" "https")))


(defclass mimetype-field (sanity-clause.field:string-field)
  ())


(defclass swagger ()
  ((swagger :field-type :constant
            :constant "2.0")
   (info :field-type :nested
         :element-type info-object
         :initarg :info
         :documentation "Provides metadata about the API. The metadata can be used by the clients if needed."
         :required t)
   (host :type string
         :initarg :host
         :documentation "The host (name or ip) serving the API. This MUST be the host only and does not include the scheme nor sub-paths. It MAY include a port. If the host is not included, the host serving the documentation is to be used (including the port). The host does not support `<https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md#pathTemplating> path templating`_.")
   (base-path :type string
              :data-key "basePath"
              :initarg :base-path
              :documentation "The base path on which the API is served, which is relative to the host. If it is not included, the API is served directly under the host. The value MUST start with a leading slash (/). The basePath does not support `<https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md#pathTemplating> path templating`_.")
   (schemes :type list
            :initarg :schemes
            :field-type :list
            :element-type scheme-field
            :documentation "The transfer protocol of the API. Values MUST be from the list: \"http\", \"https\", \"ws\", \"wss\". If the schemes is not included, the default scheme to be used is the one used to access the Swagger definition itself.")
   (consumes :type list
             :field-type :list
             :element-type mimetype-field
             :initarg :consumes
             :documentation "A list of MIME types the APIs can consume. This is global to all APIs but can be overridden on specific API calls. Value MUST be as described under `<https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md#mimeTypes> Mime Types`_.")
   (produces :type list
             :field-type :list
             :element-type mimetype-field
             :initarg :produces
             :documentation "A list of MIME types the APIs can produce. This is global to all APIs but can be overridden on specific API calls. Value MUST be as described under `<https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md#mimeTypes> Mime Types`_.")
   (paths :type list
          :initarg :paths
          :field-type :map
          :key-field :string
          :value-field path-item-object
          :documentation "")
   )
  (:metaclass sanity-clause:validated-metaclass))
