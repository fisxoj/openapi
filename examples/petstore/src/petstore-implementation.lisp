(in-package :petstore-example)

(defstruct pet
  id name tag)

(defvar *pets* (list
                (make-pet :id 1 :name "Wedge" :tag "")
                (make-pet :id 2 :name "Walter" :tag "")
                (make-pet :id 3 :name "Basil" :tag "")))

(defun pet-to-json (pet)
  (jojo:with-object
    (jojo:write-key-value "id" (pet-id pet))
    (jojo:write-key-value "name" (pet-name pet))
    (jojo:write-key-value "tag" (pet-tag pet))))


(defun make-error ()
  (jojo:with-object
    (jojo:write-key-value "code" 3)
    (jojo:write-key-value "message" "An error happened")))


(define-handler list-pets (limit)
  (list 200 '(:content-type "application/json")
        (list (jojo:with-output-to-string*
                (jojo:with-array
                  (dolist (pet (subseq *pets* 0 (min (length *pets*) limit)))
                    (jojo:write-item
                     (jojo:with-object
                       (jojo:write-key-value "id" (pet-id pet))
                       (jojo:write-key-value "name" (pet-name pet))
                       (jojo:write-key-value "tag" (pet-tag pet))))))))))


(define-handler create-pets ()
  (list 201 '(:content-type "application/json") '("null")))


(define-handler show-pet-by-id (pet-id)
  (alexandria:if-let ((pet (find pet-id *pets*
                                 :test #'=
                                 :key #'pet-id)))
    (list 200 '(:content-type "application/json")
          (list (jojo:with-output-to-string*
                  (pet-to-json pet))))
    (signal 'openapi.http-conditions:not-found)))

(export
 (defun start-server ()
   (clack:clackup *app*
                  :use-thread t
                  :port 3000)))
