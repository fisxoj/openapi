# -*- mode: restclient -*-

:port = 3000

GET http://localhost::port/pets?limit=100

#
POST http://localhost::port/pets
Content-Type: application/json
{
  "name": "Monroe",
  "id": 4,
  "tag": "potato"
}

# a pet that exists
GET http://localhost::port/pets/2

# an error
GET http://localhost::port/pets/100
