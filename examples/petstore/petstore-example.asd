(defsystem petstore-example
  :defsystem-depends-on ("openapi/server/ningle")
  :depends-on ("clack")
  :pathname "src"
  :components ((:openapi/server/ningle "petstore"
                :package "petstore-example"
                :create-package t
                :app-symbol "*app*")
               (:file "petstore-implementation")))
