(defsystem openapi
  :author "Matt Novenstern"
  :version "0.0.1"
  :depends-on ("alexandria"
               "assoc-utils"
               "cl-ppcre"
               "cl-yaml"
               "dexador"
               "jonathan"
               "json-schema"
               "str"
               "sanity-clause")
  :pathname "src"
  :components ((:file "utils")
               (:file "requests")
               (:file "http-conditions")
               ;; (:file "schema/v2")
               (:file "groveller"))
  :in-order-to ((test-op (test-op openapi/test))))


(defsystem openapi/server
  :depends-on ("openapi")
  :pathname "src/server"
  :components ((:file "server")))


(defsystem openapi/server/ningle
  :depends-on ("openapi/server"
               "ningle")
  :pathname "src/server"
  :components ((:file "ningle")))


(defsystem openapi/test
  :depends-on ("openapi"
               "rove")
  :pathname "t/"
  :components ((:file "rove-junit-reporter")
               (:file "utils")
               (:file "requests")
               (:file "groveller")
               (:file "schema/v2"))
  :perform (test-op (op c) (uiop:symbol-call :rove/reporter/junit :run c)))


(defsystem openapi/client
  :depends-on ("openapi")
  :pathname "src/client"
  :components ((:file "client")))


(defsystem openapi/client/dex
  :depends-on ("dexador"
               "openapi/client")
  :pathname "src/client"
  :components ((:file "dex")))
