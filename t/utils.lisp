(defpackage openapi/test.utils
  (:use :cl :alexandria :rove))

(in-package :openapi/test.utils)

(deftest kebab-identifier
  (ok (string= (openapi.utils:kebab-identifier "DeleteAccountsAccount") "DELETE-ACCOUNTS-ACCOUNT")
      "converts upper-camel-case to kebab.")

  (ok (string= (openapi.utils:kebab-identifier "groups_info") "GROUPS-INFO")
      "converts snake_case to kebab."))
