(defpackage openapi/test.groveller
  (:use :cl :alexandria :rove))

(in-package :openapi/test.groveller)

(deftest find-verb
  (ok (equal (openapi.groveller::find-verb "post") :post)
      "finds a verb that is allowed.")

  (ok (null (openapi.groveller::find-verb "possum"))
      "dosen't find a verb that is not allowed."))
