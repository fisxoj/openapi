(defpackage openapi/test.requests
  (:use :cl :alexandria :rove))

(in-package :openapi/test.requests)

(deftest interpolated-path
  (testing "a path with no variables"

    (multiple-value-bind (path-segments variables) (openapi.requests::interpolated-path "/episodes/latest")
      (ok (string= (first path-segments) "/episodes/latest")
          "has only one path segment.")

      (ok (null variables)
          "captures no variables.")))

  (testing "a path with two variables"

    (multiple-value-bind (path-segments variables) (openapi.requests::interpolated-path "/group/{groupId}/members/{memberId}")
      (ok (string= (first path-segments) "/group/"))
      (ok (string= (second path-segments) "/members/"))

      (ok (string= (first variables) "GROUP-ID"))
      (ok (string= (second variables) "MEMBER-ID"))))

  (testing "a path with a trailing string"
    (multiple-value-bind (path-segments variables) (openapi.requests::interpolated-path "/group/{group-id}/members/{member-id}/data")
      (ok (= (length path-segments) 3))
      (ok (= (length variables) 2))))

  (testing "a path with an unclosed interpolation variable errors"
    (ok (signals (openapi.requests::interpolated-path "/group/{group-id}/members/{member-id"))
        "at the end of the path.")
    (ok (signals (openapi.requests::interpolated-path "/group/{group-id/members/"))
        "in the middle of the path.")
    (skip ;;(signals (openapi.requests::interpolated-path "{group-id/members/{member-id}"))
     "at the beginning of the path.")))
