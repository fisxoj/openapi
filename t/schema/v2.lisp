(defpackage openapi/test/schema/v2
  (:use :cl :rove))

(in-package :openapi/test/schema/v2)

(deftest test-info
  (testing "Info Object"
    (let ((v2-info (alexandria:read-file-into-string (asdf:system-relative-pathname :openapi/test "t/schema/v2-info.json"))))

      (skip "not implemented yet")
      ;; (ng (signals (sanity-clause:load (find-class 'openapi/schema/v2:info-object) (jojo:parse v2-info :as :alist)))
      ;;     "loads from json data")
      )))
