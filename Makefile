LISP ?= sbcl --non-interactive --eval '(push (uiop:getcwd) asdf:*central-registry*)'

test:
	$(LISP) --eval "(ql:quickload :openapi)" \
		--eval "(asdf:load-system :openapi/test)" \
		--eval "(uiop:quit (if (uiop:symbol-call :rove/reporter/junit :run (asdf:find-system :openapi/test)) 0 -1))"

.PHONY: test
